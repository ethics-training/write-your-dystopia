# Write Your Dystopia (ethics training, with action ;-)

This repository was created to store the creations of the PhD students who participated in the 2-days ethics training of the doctoral school of Université de Lorraine on Nov. 3rd nd 4th, 2022, near Nancy: [https://www.loria.fr/en/explore-the-ethical-issues-of-computer-science-research-through-dystopia/](https://www.loria.fr/en/explore-the-ethical-issues-of-computer-science-research-through-dystopia/)

24 students were involved, in 6 groups. 6 dystopias were written, in 3 languages (EN, FR, ES). 

## Authors and acknowledgment
The training was organized by a large team from LORIA, including Aurore Coince, Karën Fort, Mathieu d’Aquin, Maxime Amblard and Marc Anderson.

We had the great pleasure to welcome external experts to help us: Sarah Carter, Ilaria Tiddi and Diane Ranville for the creative writting part.

Organizing these 2 days would not have been possible without the help of the doctoral school [IAEM](http://doctorat.univ-lorraine.fr/fr/les-ecoles-doctorales/iaem/presentation), the [IMPACT](https://www.univ-lorraine.fr/lue/les-projets-impact/open-language-and-knowledge-for-citizens-olki/) project OLKi,  and of the [LORIA](https://www.loria.fr/en/).

## License
Each group has decided on a specific license for its creation, please respect it.

## Project status
The creations may evolve in time. Translations in other languages may be added too.

The training can be adapted to other configurations (other labs, other doctoral schools), please contact karen.fort@loria.fr if you're interested ([https://members.loria.fr/KFort/](https://members.loria.fr/KFort/)).
